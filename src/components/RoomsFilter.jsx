import React, { useState, useEffect } from "react";
import Title from "./Title";
import { RoomContextValue } from "../context";

const getUnique = (items, value) => {
  return [...new Set(items.map(item => item[value]))];
};

export default function RoomsFilter(props) {
  const value = RoomContextValue();

  const { searchParams, handleChange, rooms } = value;
  const uniqueTypes = ["all", ...getUnique(rooms, "type")];
  const capacityList = getUnique(rooms, "capacity");
  const [price, setPrice] = useState(searchParams.price);

  useEffect(() => {
    setPrice(searchParams.price);
  }, [searchParams.price]);

  function onPriceChange(e) {
    setPrice(e.target.value);
  }
  return (
    <section className="filter-container">
      <Title title="search rooms" />
      <form className="filter-form">
        <div className="form-group">
          <label htmlFor="type">room type</label>
          <select
            name="type"
            id="type"
            className="form-control"
            value={searchParams.type}
            onChange={handleChange}
          >
            {uniqueTypes.map((item, index) => {
              return (
                <option value={item} key={index}>
                  {item}
                </option>
              );
            })}
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="capacity">Guests</label>
          <select
            name="capacity"
            id="capacity"
            className="form-control"
            value={searchParams.capacity}
            onChange={handleChange}
          >
            {capacityList.map((item, index) => {
              return (
                <option value={item} key={index}>
                  {item}
                </option>
              );
            })}
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="price">room price ${price}</label>
          <input
            type="range"
            name="price"
            id="price"
            min={searchParams.minPrice}
            max={searchParams.maxPrice}
            value={price}
            onMouseUp={handleChange}
            onTouchEnd={handleChange}
            onChange={onPriceChange}
            className="form-control"
          />
        </div>
        <div className="form-group">
          <div className="single-extra">
            <input
              type="checkbox"
              name="breakfast"
              id="breakfast"
              checked={searchParams.breakfast}
              onChange={handleChange}
            />
            <label htmlFor="checkbox">breakfast</label>
          </div>
          <div className="single-extra">
            <input
              type="checkbox"
              name="pets"
              id="pets"
              checked={searchParams.pets}
              onChange={handleChange}
            />
            <label htmlFor="checkbox">pets</label>
          </div>
        </div>
      </form>
    </section>
  );
}

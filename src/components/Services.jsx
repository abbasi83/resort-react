import React from "react";
import Title from "./Title";

import { FaCocktail, FaHiking, FaShuttleVan, FaBeer } from "react-icons/fa";
const state = {
  services: [
    {
      icon: <FaCocktail />,
      title: "free cocktail",
      info: "Lorem ipsum dolor sit amet consectetur adipisicing elit."
    },
    {
      icon: <FaHiking />,
      title: "Endless Hiking",
      info: "Lorem ipsum dolor sit amet consectetur adipisicing elit."
    },
    {
      icon: <FaShuttleVan />,
      title: "Free shuttle",
      info: "Lorem ipsum dolor sit amet consectetur adipisicing elit."
    },
    {
      icon: <FaBeer />,
      title: "Strongest beer",
      info: "Lorem ipsum dolor sit amet consectetur adipisicing elit."
    }
  ]
};
const Services = () => {
  return (
    <section className="services">
      <Title title="services" />
      <div className="services-center">
        {state.services.map((item, index) => {
          return (
            <article className="service" key={index}>
              <span>{item.icon}</span>
              <h6>{item.title}</h6>
              <p>{item.info}</p>
            </article>
          );
        })}
      </div>
    </section>
  );
};

export default Services;

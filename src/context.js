import React, { useContext, useEffect, useState } from "react";
import client from "./contentful";
const RoomContext = React.createContext();

const formatData = items => {
  return items.map(item => {
    const id = item.sys.id;
    let images = item.fields.images.map(image => {
      return image.fields.file.url;
    });
    return { ...item.fields, images, id };
  });
};

const RoomProvider = props => {
  const [rooms, setRooms] = useState([]);
  const [searchParams, setSearchParams] = useSearchParams(rooms);
  const [filteredRooms, setFilteredRooms] = useState([...rooms]);

  const getRoom = slug => {
    return rooms.find(room => room.slug === slug);
  };

  const handleChange = event => {
    const { target } = event;
    const { type, name } = target;
    const value = type === "checkbox" ? target.checked : target.value;
    setSearchParams({ ...searchParams, [name]: value });
  };
  const getFilteredRooms = rooms => {
    let temp = [...rooms];

    temp = filterByType(temp, searchParams.type);
    temp = filterByCapacity(temp, searchParams.capacity);
    temp = filterByPrice(temp, searchParams.price);
    temp = filterByBoolean(temp, searchParams.breakfast, "breakfast");
    temp = filterByBoolean(temp, searchParams.pets, "pets");

    return temp;
  };
  const state = {
    rooms,
    featuredRooms: rooms.filter(room => room.featured === true),
    sortedRooms: rooms,
    filteredRooms,
    getRoom,
    loading: false,
    searchParams,
    handleChange,
  };

  useEffect(() => {
    async function getData() {
      try {
        let response = await client.getEntries({
          content_type: "beachresortroom",
          order: "fields.price",
        });
        let items = formatData(response.items);
        setRooms(items);
      } catch (error) {
        console.log("error", error);
      }
    }
    getData();
  }, []);
  useEffect(
    () => {
      const params = getSearchParams(rooms);
      setSearchParams(params);
    },
    [rooms]
  );

  useEffect(
    () => {
      const temp = getFilteredRooms(rooms);
      setFilteredRooms(temp);
    },
    [searchParams]
  );

  return (
    <RoomContext.Provider value={state}>{props.children}</RoomContext.Provider>
  );
};

export default RoomProvider;

export const RoomContextValue = () => {
  const value = useContext(RoomContext);
  return value;
};

function getSearchParams(rooms) {
  return {
    type: "all",
    capacity: 1,
    price: Math.max(...rooms.map(o => o.price), 0),
    minPrice: Math.min(...rooms.map(o => o.price), 0),
    maxPrice: Math.max(...rooms.map(o => o.price), 0),
    minSize: Math.min(...rooms.map(o => o.size), 0),
    maxSize: Math.max(...rooms.map(o => o.size), 0),
    breakfast: false,
    pets: false,
  };
}
function useSearchParams(rooms) {
  const params = getSearchParams(rooms);
  return useState(params);
}

function filterByType(rooms, type) {
  if (type !== "all") {
    return rooms.filter(r => r.type === type);
  }
  return rooms;
}

function filterByCapacity(rooms, capacity) {
  if (capacity !== 1) {
    return rooms.filter(r => r.capacity >= capacity);
  }
  return rooms;
}
function filterByPrice(rooms, price) {
  return rooms.filter(r => r.price <= price);
}
function filterByBoolean(rooms, flag, key) {
  if (flag) {
    return rooms.filter(r => {
      console.log(r[key]);
      return r[key] === flag;
    });
  }
  return rooms;
}

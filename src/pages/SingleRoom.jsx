import React, { useLayoutEffect } from "react";
import Banner from "../components/Banner";
import { Link } from "react-router-dom";
import { RoomContextValue } from "../context";
import StyledHero from "../components/StyledHero";

const SingleRoom = props => {
  const { getRoom } = RoomContextValue();
  const room = getRoom(props.match.params.slug);

  const {
    name,
    description,
    capacity,
    size,
    price,
    extras,
    breakfast,
    pets,
    images
  } = room;
  const [mainImage, ...restImages] = images;

  useLayoutEffect(() => {
    window.scrollTo(0, 0);
  });
  if (!room) {
    return (
      <div className="error">
        <h3>no such room could be found...</h3>
        <Link to="/rooms" className="btn-primary">
          back to rooms
        </Link>
      </div>
    );
  }

  return (
    <>
      <StyledHero img={mainImage}>
        <Banner title={`${name} room`}>
          <Link to="/rooms" className="btn-primary">
            back to rooms
          </Link>
        </Banner>
      </StyledHero>
      <section className="single-room">
        <div className="single-room-images">
          {restImages.map((image, index) => {
            return <img src={image} key={index} alt={name} />;
          })}
        </div>
        <div className="single-room-info">
          <article className="desc">
            <h3>details</h3>
            <p>{description}</p>
          </article>
          <article className="info">
            <h3>info</h3>
            <h6>price : ${price}</h6>
            <h6>size : {size} SQFT</h6>
            <h6>
              max capacity :{" "}
              {capacity > 1
                ? `${capacity} people`
                : `${capacity}
              person`}
            </h6>
            <h6>{pets ? "pets allowed" : "no pets allowed"}</h6>
            <h6>{breakfast ? "free breakfast included" : ""}</h6>
          </article>
        </div>
      </section>
      <section className="room-extras">
        <h6>extras</h6>
        <ul className="extras">
          {extras.map((item, index) => {
            return <li key={index}>- {item}</li>;
          })}
        </ul>
      </section>
    </>
  );
};

export default SingleRoom;
